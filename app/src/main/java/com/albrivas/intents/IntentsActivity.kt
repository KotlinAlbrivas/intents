/*
 * File: IntentsActivity.kt
 * Project: Intents
 *
 * Created by albrivas on 17/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 *
 */

package com.albrivas.intents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.albrivas.intents.model.Person
import com.albrivas.intents.activities.IntentExtrasActivity
import com.albrivas.intents.activities.IntentFlagActivity
import com.albrivas.intents.activities.IntentObjectActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.noAnimation

class IntentsActivity : AppCompatActivity() {

    companion object {
        const val NAME = "name"
        const val LASTNAME = "lastname"
        const val GENDER = "genero"
        const val DEVELOPER = "developer"
        const val OBJECT = "object"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar as Toolbar?)
        acciones()
    }

    private fun acciones() {
        buttonIntentExtras.setOnClickListener { goIntentExtras() }
        buttonIntentFlags.setOnClickListener { goIntentFlags() }
        buttonIntentObject.setOnClickListener { goIntentObject() }
    }


    private fun goIntentExtras() {
        var intent = intentFor<IntentExtrasActivity>(
            NAME to "Alberto",
            LASTNAME to "Rivas Ramos",
            GENDER to "Hombre",
            DEVELOPER to true)

        startActivity(intent)
    }

    private fun goIntentFlags() {
        var intent = intentFor<IntentFlagActivity>(
            NAME to "Alberto",
            LASTNAME to "Rivas Ramos",
            GENDER to "Hombre",
            DEVELOPER to true).noAnimation()

        startActivity(intent)
    }

    private fun goIntentObject() {
        var person = Person("Juan", "Gomez Garcia", "Hombre", false)
        var intent = intentFor<IntentObjectActivity>(OBJECT to person)

        startActivity(intent)
    }
}
