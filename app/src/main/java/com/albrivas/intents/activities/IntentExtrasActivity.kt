/*
 * File: IntentExtrasActivity.kt
 * Project: Intents
 *
 * Created by albrivas on 17/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 *
 */

package com.albrivas.intents.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.albrivas.intents.IntentsActivity
import com.albrivas.intents.R
import kotlinx.android.synthetic.main.activity_intent_extras.*

class IntentExtrasActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_extras)

        setSupportActionBar(toolbarExtras as Toolbar?)
        (toolbarExtras as Toolbar?)?.title = getString(R.string.title_extras)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        intent?.let {

            val name = intent.getStringExtra(IntentsActivity.NAME)
            val lastName = intent.getStringExtra(IntentsActivity.LASTNAME)
            val gender = intent.getStringExtra(IntentsActivity.GENDER)
            val developer = intent.getBooleanExtra(IntentsActivity.DEVELOPER, false)

            textName.text = name
            textLastName.text = lastName
            textGender.text = gender
            checkDeveloper.isChecked = developer
        }

    }
}

