/*
 * File: IntentObjectActivity.kt
 * Project: Intents
 *
 * Created by albrivas on 17/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 *
 */

package com.albrivas.intents.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.albrivas.intents.IntentsActivity
import com.albrivas.intents.R
import com.albrivas.intents.model.Person
import kotlinx.android.synthetic.main.activity_intent_object.*

class IntentObjectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_object)

        setSupportActionBar(toolbarObject as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (toolbarObject as Toolbar?)!!.title = getString(R.string.title_object)


        var person = intent.getParcelableExtra<Person>(IntentsActivity.OBJECT)

        person?.let {

            textName.text = person.name
            textLastName.text = person.lastName
            textGender.text = person.gender
            checkDeveloper.isChecked = person.developer
        }

    }
}
