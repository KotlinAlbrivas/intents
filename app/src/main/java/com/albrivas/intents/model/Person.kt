/*
 * File: Person.kt
 * Project: Intents
 *
 * Created by albrivas on 17/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 *
 */

package com.albrivas.intents.model

import android.os.Parcel
import android.os.Parcelable

data class Person (
    var name: String = "Alberto",
    var lastName: String = "Rivas",
    var gender: String = "Hombre",
    var developer: Boolean = true) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(lastName)
        parcel.writeString(gender)
        parcel.writeByte(if (developer) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Person> {
        override fun createFromParcel(parcel: Parcel): Person {
            return Person(parcel)
        }

        override fun newArray(size: Int): Array<Person?> {
            return arrayOfNulls(size)
        }
    }
}